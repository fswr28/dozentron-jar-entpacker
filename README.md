# dozentron-jar-entpacker

Entpacker Tool für Abgaben die mit Dozentron https://git.thm.de/dalt40/dozentron eingereicht wurden.
## Vorrausetzungen
- Linux oder LinuxSubsystem
- unzip muss installiert sein

## Usage
- jar_entpacker.sh mit chmod ausführbar machen
- ./jar_entpacker.sh <Archivname\>

Das Archiv wird in den Ordner <Archivname\> entpackt.

## Todo
 + Support für Windows?
 + Support für Leerzeichen im Pfad/Dateiname
Umstellung auf andere Programmiersprache ?


## Known Issues
- Leerzeichen im Pfad oder Ordnernamen lassen Skript abstürzen
